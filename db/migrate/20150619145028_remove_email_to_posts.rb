class RemoveEmailToPosts < ActiveRecord::Migration
  def change
    remove_column :posts, :email, :varchar
  end
end
